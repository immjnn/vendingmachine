import React, { useState, useEffect } from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet } from 'react-native';
import { getProduct, saveProduct } from '../services/storage';

const ProductList = ({onSelected, data}) => {
  const [selected, setSelected] = useState(null);
  // const [products, setProducts] = useState([]);

  // useEffect(() => {
  //   getProduct().then(res => {
  //     if (res.length == 0) {
  //       saveProduct().then(p => {
  //         getProduct().then(p2 => {
  //           setProducts(p2)
  //         })
  //       })
  //     }else{
  //       setProducts(res)
  //     }
  //   })
  // }, [])

  const itemSelected = (item) => {
    setSelected(item)
    onSelected(item)
  }
  return (
    <FlatList
      keyExtractor={item => item.id.toString()}
      data={data}
      renderItem={({item}) => (
        <TouchableOpacity onPress={() => itemSelected(item)} style={[styles.card, {backgroundColor: selected && selected.id == item.id ? 'darkgrey' : 'lightgrey'}]}>
          <Text style={{fontWeight: 'bold'}}>{item.name}</Text>
          <Text>Price: {item.price}</Text>
          <Text>Available: {item.available}</Text>
        </TouchableOpacity>
      )}
      numColumns={3}
    />
  )
}

const styles = StyleSheet.create({
  card: {
    height: 100,
    width: `${100/3}%`,
    marginRight: 2,
    marginBottom: 2,
    padding: 12
  }
})

export default ProductList