import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const Header = ({title='Title'}) => {
  return (
    <View style={styles.header}>
      <Text style={{fontSize: 18}}>{title}</Text>
    </View>
  )
}

const styles = StyleSheet.create({
  header: {
    height: 56, 
    width: '100%', 
    backgroundColor: 'white',
    elevation: 4,
    justifyContent: 'center',
    padding: 12
  }
})

export default Header