import React, { useState } from 'react';
import { View, FlatList, Text, TouchableOpacity, StyleSheet } from 'react-native';

const NumberInput = ({onSelected}) => {
  const [selected, setSelected] = useState(null);
  const itemSelected = (item) => {
    setSelected(item)
    onSelected(item)
  }
  return (
    <FlatList
      keyExtractor={item => item.toString()}
      data={[2000, 5000, 10000, 20000, 50000]}
      renderItem={({item}) => (
        <TouchableOpacity onPress={() => itemSelected(item)} style={[styles.card, {backgroundColor: selected == item ? 'darkgrey' : 'lightgrey'}]}>
          <Text style={{color: 'black'}}>{item}</Text>
        </TouchableOpacity>
      )}
      horizontal={true}
    />
  )
}

const styles = StyleSheet.create({
  card: {
    padding: 12,
    marginRight: 2,
  }
});

export default NumberInput