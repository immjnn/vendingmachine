import React, { Component } from 'react';
import { View, Text, FlatList, TouchableOpacity, Button, Alert } from 'react-native';
import { Header, NumberInput, ProductList } from '../components';
import { getProduct, saveProduct, updateProductById } from '../services/storage';

export default class HomeScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      products: [],
      productSelected: null,
      numberSelected: null,
      isChange: false
    };
  }

  componentDidMount() {
    this.loadProduct()
  }

  onResetData = () => {
    saveProduct().then(p => {
      getProduct().then(p2 => {
        this.setState({products: p2})
      })
    })
  }

  loadProduct = () => {
    getProduct().then(res => {
      if (res.length == 0) {
        saveProduct().then(p => {
          getProduct().then(p2 => {
            this.setState({products: p2})
          })
        })
      }else{
        this.setState({products: res})
      }
    })
  }

  onProcess = () => {
    const { productSelected, numberSelected } = this.state
    if(!productSelected || !numberSelected){
      Alert.alert('Failed', 'Please select product and money')
    }else{
      const data = {
        id: productSelected.id,
        name: productSelected.name,
        available: productSelected.available-1,
        price: productSelected.price
      }
      if(productSelected.available == 0){
        Alert.alert('Failed', 'Sorry, this product is empty')
      }else if(productSelected.price > numberSelected){
        Alert.alert('Failed', 'Your money is not enough')
      }else{
        updateProductById(productSelected.id, data).then(res => {
          this.loadProduct()
          Alert.alert('Success' ,`Payment Successful :D, money change: ${numberSelected - productSelected.price}`)
        })
      }
    }
  }

  render() {
    return (
      <View style={{flex: 1}}>
        <Header title='Vending Machine'/>
        <View style={{padding: 12}}>
          <Button
            title='Reset Data'
            color='red'
            onPress={() => this.onResetData()}
          />
          <View style={{paddingBottom: 12}}>
            <Text style={{fontWeight: 'bold', fontSize: 18}}>Select Product</Text>
            <ProductList
              data={this.state.products}
              onSelected={p => this.setState({productSelected: p})}
            />
          </View>
          <View style={{paddingBottom: 12}}>
            <Text style={{fontWeight: 'bold', fontSize: 18}}>Amount of Payment </Text>
            <NumberInput
              onSelected={p => this.setState({numberSelected: p})}
            />
          </View>
          <Button
            title='Process'
            color='green'
            onPress={() => this.onProcess()}
          />
        </View>
      </View>
    );
  }
}
