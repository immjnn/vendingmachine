import Storage from 'react-native-storage';
import AsyncStorage from '@react-native-community/async-storage';
import { products } from '../utils/static';

const storage = new Storage({
  // maximum capacity, default 1000
  size: 9000,

  // Use AsyncStorage for RN apps, or window.localStorage for web apps.
  // If storageBackend is not set, data will be lost after reload.
  storageBackend: AsyncStorage, // for web: window.localStorage

  // expire time, default: 1 day (1000 * 3600 * 24 milliseconds).
  // can be null, which means never expire.
  defaultExpires: null,

  // cache data in the memory. default is true.
  enableCache: true,

  // if data was not found in storage or expired data was found,
  // the corresponding sync method will be invoked returning
  // the latest data.
  sync: {
    // we'll talk about the details later.
  }
});

export const saveProduct = async () => {
  let response = products.map(item => {
    storage.save({key: 'productItem', id: item.id, data: item})
  });
  return response
}

export const getProduct = async () => {
  let response = await storage.getAllDataForKey('productItem')
  return response
}

export const updateProductById = async (id, item) => {
  return storage.save({key: 'productItem', id: id, data: item})
}