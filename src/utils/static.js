export const products = [
  {
    id: 1,
    name: 'Biskuit',
    available: 8,
    price: 6000
  },
  {
    id: 2,
    name: 'Chips',
    available: 12,
    price: 8000
  },
  {
    id: 3,
    name: 'Oreo',
    available: 6,
    price: 10000
  },
  {
    id: 4,
    name: 'Tango',
    available: 12,
    price: 12000
  },
  {
    id: 5,
    name: 'Cokelat',
    available: 4,
    price: 15000
  },
]