/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, { useEffect } from 'react';
import HomeScreen from './src/screens/HomeScreen';
import { saveProduct, getProduct } from './src/services/storage';

const App = () => {

  useEffect(() => {
    getProduct().then(res => {
      if (res.length == 0) {
        saveProduct()
      }
    })
  }, [])

  return (
    <HomeScreen/>
  );
};

export default App;
