### Installation
```sh
$ cd vendingmachine
$ npm install
```

### Run or Build Android
```sh
$ npm run android
```